from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.conf import settings

from .views import *
from .models import *


# Create your tests here.
class Story7PPWUnitTest(TestCase):

    def test_profile_url_is_exist(self):
        response = Client().get('/accordionPage/')
        self.assertEqual(response.status_code,200)
    
    def test_profile_url_doesnt_exist(self):
        response = Client().get('/none')
        self.assertEqual(response.status_code, 404)

    def test_profile_using_index_template(self):
        response = Client().get('/accordionPage/')
        self.assertTemplateUsed(response, 'indexpage.html')



